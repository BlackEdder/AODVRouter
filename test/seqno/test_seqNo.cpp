#ifdef UNIT_TEST

#include <unity.h>
#include <AODVNode.h>


/**
6.1. Maintaining Sequence Numbers

   Every route table entry at every node MUST include the latest
   information available about the sequence number for the IP address of
   the destination node for which the route table entry is maintained.
   This sequence number is called the "destination sequence number".  It
   is updated whenever a node receives new (i.e., not stale) information
   about the sequence number from RREQ, RREP, or RERR messages that may
   be received related to that destination.  AODV depends on each node
   in the network to own and maintain its destination sequence number to
   guarantee the loop-freedom of all routes towards that node.  A
   destination node increments its own sequence number in two
   circumstances:

   -  Immediately before a node originates a route discovery, it MUST
      increment its own sequence number.  This prevents conflicts with
      previously established reverse routes towards the originator of a
      RREQ.
*/
void test_seqno_on_rreq() {
    AODVNode node(0);

    TEST_ASSERT_EQUAL(1, node.seqNo);

    node.initializeRREQ();
    TEST_ASSERT_EQUAL(2, node.seqNo);
}

/**
   -  Immediately before a destination node originates a RREP in
      response to a RREQ, it MUST update its own sequence number to the
      maximum of its current sequence number and the destination
      sequence number in the RREQ packet.
*/
void test_seqno_on_rrep() {
    AODVNode node(0);

    TEST_ASSERT_EQUAL(1, node.seqNo);

    RREQPackage rreq(10, 1, 1, 1, 2, 5, 4);
    node.initializeRREP(rreq);
    TEST_ASSERT_EQUAL(1, node.seqNo);

    // Take into account seq number send by rreq
    rreq.destinationSeqNo = 4;
    node.initializeRREP(rreq);
    TEST_ASSERT_EQUAL(4, node.seqNo);

    // Wrap around
    node.seqNo = 4294967295;
    node.initializeRREP(rreq);
    TEST_ASSERT_EQUAL(4, node.seqNo);
}


/**
   When the destination increments its sequence number, it MUST do so by
   treating the sequence number value as if it were an unsigned number.
   To accomplish sequence number rollover, if the sequence number has
   already been assigned to be the largest possible number representable
   as a 32-bit unsigned integer (i.e., 4294967295), then when it is
   incremented it will then have a value of zero (0).  On the other
   hand, if the sequence number currently has the value 2147483647,
   which is the largest possible positive integer if 2's complement
   arithmetic is in use with 32-bit integers, the next value will be
   2147483648, which is the most negative possible integer in the same
   numbering system.  The representation of negative numbers is not
   relevant to the increment of AODV sequence numbers.  This is in
   contrast to the manner in which the result of comparing two AODV
   sequence numbers is to be treated (see below).
*/

void test_rollover_seqno() {
    AODVNode node(0);
    node.seqNo = 4294967295;
    TEST_ASSERT_EQUAL(4294967295, node.seqNo);
    ++node.seqNo;
    TEST_ASSERT_EQUAL(0, node.seqNo);
}

/**
   In order to ascertain that information about a destination is not
   stale, the node compares its current numerical value for the sequence
   number with that obtained from the incoming AODV message.  This
   comparison MUST be done using signed 32-bit arithmetic, this is
   necessary to accomplish sequence number rollover.  If the result of
   subtracting the currently stored sequence number from the value of
   the incoming sequence number is less than zero, then the information
   related to that destination in the AODV message MUST be discarded,
   since that information is stale compared to the node's currently
   stored information.
*/
void test_compare_seqno() {
    AODVNode node1(0);
    node1.seqNo = 4294967295;
    AODVNode node2(1);
    node2.seqNo = 4294967295 + 1;
    TEST_ASSERT(node1.seqNo > node2.seqNo); // We do indeed overflow
    TEST_ASSERT(seqNo_lower_than(node1.seqNo, node2.seqNo));

    node1.seqNo = 4294967295;
    node2.seqNo = 4294967295 - 1;
    TEST_ASSERT(seqNo_lower_than(node2.seqNo, node1.seqNo));

    node1.seqNo = 4294967294/2 + 1000;
    node2.seqNo = 4294967294/2;
    TEST_ASSERT(seqNo_lower_than(node2.seqNo, node1.seqNo));
}

/**
   NEEDED: NEED ROUTETABLE

   The only other circumstance in which a node may change the
   destination sequence number in one of its route table entries is in
   response to a lost or expired link to the next hop towards that
   destination.  The node determines which destinations use a particular
   next hop by consulting its routing table.  In this case, for each
   destination that uses the next hop, the node increments the sequence
   number and marks the route as invalid (see also sections 6.11, 6.12).
   Whenever any fresh enough (i.e., containing a sequence number at
   least equal to the recorded sequence number) routing information for
   an affected destination is received by a node that has marked that
   route table entry as invalid, the node SHOULD update its route table
   information according to the information contained in the update.


   A node may change the sequence number in the routing table entry of a
   destination only if:

   -  it is itself the destination node, and offers a new route to
      itself, or

   -  it receives an AODV message with new information about the
      sequence number for a destination node, or

   -  the path towards the destination node expires or breaks.
*/
void setup() {
    UNITY_BEGIN();    // IMPORTANT LINE!
}

void loop() {
    RUN_TEST(test_seqno_on_rreq);
    RUN_TEST(test_seqno_on_rrep);
    RUN_TEST(test_rollover_seqno);
    RUN_TEST(test_compare_seqno);
    UNITY_END(); // stop unit testing
}
#endif
