#include <AODVRouter.h>
#include <unity.h>

#ifdef UNIT_TEST
uint8_t uint8_max = 255; // 2^8 - 1
uint16_t uint16_max = 65536;
uint32_t uint32_max = 4294967295;

void test_setup() {
    // Do we actually overflow?
    TEST_ASSERT(uint32_max > uint32_max + 1);
}

void test_find_connection_in_route_table() {
    Route r = Route(0, 1, 2, 0);
    RouteTable rt;
    TEST_ASSERT_EQUAL(rt.findRouteToDestination(0, 1000), NULL);

    rt.insert(r);
    TEST_ASSERT_EQUAL(rt.findRouteToDestination(0, 1000)->nextHopID, 1);
    TEST_ASSERT_EQUAL(rt.routeTable.size(), 1);

    // Test expired
    TEST_ASSERT_EQUAL(NULL, rt.findRouteToDestination(0, 1000*1000*700)); 
    TEST_ASSERT_EQUAL(0, rt.routeTable.size());

    // Test time overflow
    Route r2 = Route(0, 1, 2, uint32_max);
    rt.insert(r2);
    TEST_ASSERT_EQUAL(rt.findRouteToDestination(0, 1000)->nextHopID, 1);
    TEST_ASSERT_EQUAL(rt.routeTable.size(), 1);
}

void setup() {
    UNITY_BEGIN();    // IMPORTANT LINE!
}

void loop() {
    RUN_TEST(test_setup);
    RUN_TEST(test_find_connection_in_route_table);
    UNITY_END(); // stop unit testing
}
#endif
