#ifdef UNIT_TEST

#include <unity.h>
#include <AODVRoute.h>
#include <AODVNode.h>

/**
6.2. Route Table Entries and Precursor Lists

   When a node receives an AODV control packet from a neighbor, or
   creates or updates a route for a particular destination or subnet, it
   checks its route table for an entry for the destination.  In the
   event that there is no corresponding entry for that destination, an
   entry is created.  The sequence number is either determined from the
   information contained in the control packet, or else the valid
   sequence number field is set to false.  The route is only updated if
   the new sequence number is either

   (i)       higher than the destination sequence number in the route
             table, or
*/
void test_newer_route() {
    RouteTable rt;
    // Test calling route table tryUpdate directly
    //    bool tryUpdate( uint32_t destinationID, uint32_t destinationSeqNo, uint32_t hopCount, 
    //        uint32_t nextHopID, uint32_t lifeTime) {
    TEST_ASSERT(rt.tryUpdate(1, 1, 5, 2, 0));
    TEST_ASSERT_EQUAL(1, rt.table.size());

    TEST_ASSERT(!rt.tryUpdate(1, 1, 5, 2, 0));
    TEST_ASSERT_EQUAL(1, rt.table.size());

    TEST_ASSERT(rt.tryUpdate(1, 2, 5, 2, 0));
    TEST_ASSERT_EQUAL(1, rt.table.size());
}

/**
   (ii)      the sequence numbers are equal, but the hop count (of the
             new information) plus one, is smaller than the existing hop
             count in the routing table, or
*/

void test_shorter_route() {
    RouteTable rt;
    TEST_ASSERT(rt.tryUpdate(1, 1, 5, 2, 0));
    TEST_ASSERT_EQUAL(1, rt.table.size());

    TEST_ASSERT(!rt.tryUpdate(1, 1, 6, 2, 0));
    TEST_ASSERT_EQUAL(1, rt.table.size());

    TEST_ASSERT(rt.tryUpdate(1, 1, 4, 2, 0));
    TEST_ASSERT_EQUAL(1, rt.table.size());
}

/**
   (iii)     the sequence number is unknown.

    NOTE: As far as I can tell this should never happen.
*/



/**
   The Lifetime field of the routing table entry is either determined
   from the control packet, or it is initialized to
   ACTIVE_ROUTE_TIMEOUT.  This route may now be used to send any queued
   data packets and fulfills any outstanding route requests.
*/
void test_lifetime_route() {
    AODVNode node(1);

    TEST_ASSERT_EQUAL(0, node.routeTable.table.size());

    // Test node receiving a RREQ
    //    RREQPackage(uint32_t rID, uint32_t destID, uint32_t destSeqNo, uint32_t origID,
    //        uint32_t origSeqNo, uint32_t hopC, uint32_t lID)
    RREQPackage rreq(10, 11, 12, 1, 2, 5, 4);
    node.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(1, node.routeTable.table.size());
    auto lt = node.routeTable.table.begin()->lifetime;
    TEST_ASSERT(lt >= millis());
    TEST_ASSERT(lt <= millis() + ACTIVE_ROUTE_TIMEOUT);
 
    // Test node receiving a RREP
    //    RREPPackage(uint32_t destID, uint32_t destSeqNo, uint32_t origID,
    //        uint32_t hopC, uint32_t lID, uint32_t life)
    RREPPackage rrep(11, 12, 1, 5, 4, ACTIVE_ROUTE_TIMEOUT);
    node.receiveRREP(rrep);
    TEST_ASSERT_EQUAL(2, node.routeTable.table.size());
    TEST_ASSERT_EQUAL(11, (node.routeTable.table.end()-1)->destinationID);
    lt = (node.routeTable.table.end()-1)->lifetime;
    TEST_ASSERT(lt >= millis());
    TEST_ASSERT(lt <= millis() + ACTIVE_ROUTE_TIMEOUT);
     
    // NEEDED?: what about RRER packages?
}

/**
 * NEEDED:
 *
 * NOTE that this refers to a data packet, not control packets...
   Each time a route is used to forward a data packet, its Active Route
   Lifetime field of the source, destination and the next hop on the
   path to the destination is updated to be no less than the current
   time plus ACTIVE_ROUTE_TIMEOUT.  Since the route between each
   originator and destination pair is expected to be symmetric, the
   Active Route Lifetime for the previous hop, along the reverse path
   back to the IP source, is also updated to be no less than the current
   time plus ACTIVE_ROUTE_TIMEOUT.  The lifetime for an Active Route is
   updated each time the route is used regardless of whether the
   destination is a single node or a subnet.

   For each valid route maintained by a node as a routing table entry,
   the node also maintains a list of precursors that may be forwarding
   packets on this route.  These precursors will receive notifications
   from the node in the event of detection of the loss of the next hop
   link.  The list of precursors in a routing table entry contains those
   neighboring nodes to which a route reply was generated or forwarded.
*/

void setup() {
    UNITY_BEGIN();    // IMPORTANT LINE!
}

void loop() {
    RUN_TEST(test_newer_route);
    RUN_TEST(test_shorter_route);
    RUN_TEST(test_lifetime_route);
    UNITY_END(); // stop unit testing
}

#endif
