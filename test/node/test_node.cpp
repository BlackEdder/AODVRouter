#ifdef UNIT_TEST

#include <unity.h>
#include <AODVNode.h>

/**
6.5. Processing and Forwarding Route Requests

   UNIMPLEMENTED: we'll treat neigbouring routes exactly same as longer distances for now.

   When a node receives a RREQ, it first creates or updates a route to
   the previous hop without a valid sequence number (see section 6.2)
*/

/*
   then checks to determine whether it has received a RREQ with the same
   Originator IP Address and RREQ ID within at least the last
   PATH_DISCOVERY_TIME.  If such a RREQ has been received, the node
   silently discards the newly received RREQ.  The rest of this
   subsection describes actions taken for RREQs that are not discarded.
*/
void test_discard_rreq() {
    AODVNode node(1);
    // Is first one added to list?
    RREQPackage rreq(10, 11, 12, 1, 2, 5, 4);
    node.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(1, node.rreqCache.size());
    TEST_ASSERT(node.rreqCache.begin()->match(rreq, millis()));

    node.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(1, node.rreqCache.size());
}

/*
   First, it first increments the hop count value in the RREQ by one, to
   account for the new hop through the intermediate node.  Then the node
   searches for a reverse route to the Originator IP Address (see
   section 6.2), using longest-prefix matching.  If need be, the route
   is created, or updated using the Originator Sequence Number from the
   RREQ in its routing table.  This reverse route will be needed if the
   node receives a RREP back to the node that originated the RREQ
   (identified by the Originator IP Address).  When the reverse route is
   created or updated, the following actions on the route are also
   carried out:

   1. the Originator Sequence Number from the RREQ is compared to the
      corresponding destination sequence number in the route table entry
      and copied if greater than the existing value there

   2. the valid sequence number field is set to true; NOT IMPLEMENTED. We use lifetime for that

   3. the next hop in the routing table becomes the node from which the
      RREQ was received (it is obtained from the source IP address in
      the IP header and is often not equal to the Originator IP Address
      field in the RREQ message);

   4. the hop count is copied from the Hop Count in the RREQ message;
*/
void test_update_routetable_on_rreq() {
    AODVNode node(1);
    // Is first one added to list?
    RREQPackage rreq(10, 11, 12, 1, 2, 5, 4);
    node.receiveRREQ(rreq);
    auto newRoute = node.routeTable.table.begin();
    TEST_ASSERT_EQUAL(1, newRoute->destinationID);
    TEST_ASSERT_EQUAL(2, newRoute->destinationSeqNo);
    TEST_ASSERT_EQUAL(6, newRoute->hopCount);
    TEST_ASSERT(millis() <= newRoute->lifetime);
}

/*
 * NEEDED: Currently we use ACTIVE_TIM... as suggest in the routetable part of the document.
 * Maybe we should use schema below
 *
   Whenever a RREQ message is received, the Lifetime of the reverse
   route entry for the Originator IP address is set to be the maximum of
   (ExistingLifetime, MinimalLifetime), where

      MinimalLifetime =    (current time + 2*NET_TRAVERSAL_TIME -
                           2*HopCount*NODE_TRAVERSAL_TIME).

   The current node can use the reverse route to forward data packets in
   the same way as for any other route in the routing table.

   If a node does not generate a RREP (following the processing rules in
   section 6.6), and if the incoming IP header has TTL larger than 1,
   the node updates and broadcasts the RREQ to address 255.255.255.255
   on each of its configured interfaces (see section 6.14).  To update
   the RREQ, the TTL or hop limit field in the outgoing IP header is
   decreased by one, and the Hop Count field in the RREQ message is
   incremented by one, to account for the new hop through the
   intermediate node.  Lastly, the Destination Sequence number for the
   requested destination is set to the maximum of the corresponding
   value received in the RREQ message, and the destination sequence
   value currently maintained by the node for the requested destination.
   However, the forwarding node MUST NOT modify its maintained value for
   the destination sequence number, even if the value received in the
   incoming RREQ is larger than the value currently maintained by the
   forwarding node.
*/

size_t cb1Cnt = 0;
RREQPackage cbRREQ;
void cb1(const RREQPackage &rreq) {
    ++cb1Cnt;
    cbRREQ = rreq;
}

void test_forward_rreq() {
    // Test that sendNeighboursRREQCB is called
    AODVNode node(1);
    node.setSendNeighboursRREQCallback(&cb1);
    // Is first one added to list?
    RREQPackage rreq(10, 11, 12, 1, 2, 5, 4);
    node.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(1, cb1Cnt);

    node.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(1, cb1Cnt);
 
    // Test that destination sequence number is appropiately increase/decreased,
    // but kept unchanged on the node
    //Route(uint32_t destID, uint32_t destSeqNo, uint32_t hopC, 
    //        uint32_t hopID, uint32_t ltime) {
    AODVNode node2(1);
    node2.setSendNeighboursRREQCallback(&cb1);
    node2.routeTable.table.push_front(Route(11, 10, 5, 2, millis() - 1));
    node2.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(12, cbRREQ.destinationSeqNo);
    TEST_ASSERT_EQUAL(1, cbRREQ.lastID);
    TEST_ASSERT_EQUAL(10, node2.routeTable.table.begin()->destinationSeqNo);

    AODVNode node3(111);
    node3.setSendNeighboursRREQCallback(&cb1);
    rreq.destinationSeqNo = 8;
    cbRREQ = rreq;
    node3.routeTable.table.push_front(Route(11, 10, 5, 2, millis() - 1));
    node3.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(3, cb1Cnt);
    TEST_ASSERT_EQUAL(10, rreq.destinationSeqNo);
    TEST_ASSERT_EQUAL(10, cbRREQ.destinationSeqNo);
    TEST_ASSERT_EQUAL(10, node3.routeTable.table.begin()->destinationSeqNo);
    TEST_ASSERT_EQUAL(111, cbRREQ.lastID);
}

/**
 * NOT IMPLEMENTED all the details below
   Otherwise, if a node does generate a RREP, then the node discards the
   RREQ. Notice that, if intermediate nodes reply to every transmission
   of RREQs for a particular destination, it might turn out that the
   destination does not receive any of the discovery messages.  In this
   situation, the destination does not learn of a route to the
   originating node from the RREQ messages.  This could cause the
   destination to initiate a route discovery (for example, if the
   originator is attempting to establish a TCP session).  In order that
   the destination learn of routes to the originating node, the
   originating node SHOULD set the "gratuitous RREP" ('G') flag in the
   RREQ if for any reason the destination is likely to need a route to
   the originating node.  If, in response to a RREQ with the 'G' flag
   set, an intermediate node returns a RREP, it MUST also unicast a
   gratuitous RREP to the destination node (see section 6.6.3).

6.6. Generating Route Replies

   A node generates a RREP if either:

   (i)       it is itself the destination, or

   (ii)      it has an active route to the destination, the destination
             sequence number in the node's existing route table entry
             for the destination is valid and greater than or equal to
             the Destination Sequence Number of the RREQ (comparison
             using signed 32-bit arithmetic), and the "destination only"
             ('D') flag is NOT set.

   When generating a RREP message, a node copies the Destination IP
   Address and the Originator Sequence Number from the RREQ message into
   the corresponding fields in the RREP message.  Processing is slightly
   different, depending on whether the node is itself the requested
   destination (see section 6.6.1), or instead if it is an intermediate
   node with an fresh enough route to the destination (see section
   6.6.2).

   Once created, the RREP is unicast to the next hop toward the
   originator of the RREQ, as indicated by the route table entry for
   that originator.  As the RREP is forwarded back towards the node
   which originated the RREQ message, the Hop Count field is incremented
   by one at each hop.  Thus, when the RREP reaches the originator, the
   Hop Count represents the distance, in hops, of the destination from
   the originator.
   */
size_t cb2Cnt = 0;
RREPPackage cbRREP;
uint32_t cbDestID;
void cb2(uint32_t destID, const RREPPackage &rrep) {
    ++cb2Cnt;
    cbRREP = rrep;
    cbDestID = destID;
}

void test_reply_with_rrep() {
    // Generate RREP if it is destination
    AODVNode node(1);
    node.setSendNeighbourRREPCallback(&cb2);
    RREQPackage rreq(10, 1, 12, 1, 2, 5, 4);
    node.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(1, cb2Cnt);

    // It already has an _active_ route
    node.id = 11;
    ++rreq.rreqID;
    node.routeTable.table.push_front(Route(1, 13, 5, 2, millis() + 1000));
    node.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(2, cb2Cnt);

    // It has invalid/non lifetime route (also check then RREQ is forwarded)
    // Its destinationSeqNo is out of date
    AODVNode node2(11);
    node2.routeTable.table.push_front(Route(1, 13, 5, 2, millis() - 1));
    cb1Cnt = 0;
    node2.setSendNeighboursRREQCallback(&cb1);
    node2.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(2, cb2Cnt);
    TEST_ASSERT_EQUAL(1, cb1Cnt);

    AODVNode node3(11);
    node3.routeTable.table.push_front(Route(1, 10, 5, 2, millis() + 1000));
    cb1Cnt = 0;
    node3.setSendNeighboursRREQCallback(&cb1);
    node3.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(2, cb2Cnt);
    TEST_ASSERT_EQUAL(1, cb1Cnt);
}

/**
 * The information on sequence number in section 6.6.1 disagrees with 
 * information in another part. Since the other part makes more sense
 * we will keep that behaviour
 *
 -  Immediately before a destination node originates a RREP in
      response to a RREQ, it MUST update its own sequence number to the
      maximum of its current sequence number and the destination
      sequence number in the RREQ packet.
 *
6.6.1. Route Reply Generation by the Destination

INVALID: If the generating node is the destination itself, it MUST increment
   its own sequence number by one if the sequence number in the RREQ
   packet is equal to that incremented value.  Otherwise, the
   destination does not change its sequence number before generating the
   RREP message.  
   
   The destination node places its (perhaps newly
   incremented) sequence number into the Destination Sequence Number
   field of the RREP, and enters the value zero in the Hop Count field
   of the RREP.

   The destination node copies the value MY_ROUTE_TIMEOUT (see section
   10) into the Lifetime field of the RREP.  Each node MAY reconfigure
   its value for MY_ROUTE_TIMEOUT, within mild constraints (see section
   10).
*/
void test_reply_with_rrep_when_destination() {
    cb2Cnt = 0;
    // Generate RREP if it is destination
    AODVNode node(1);
    auto oldSq = node.seqNo;
    node.setSendNeighbourRREPCallback(&cb2);
    RREQPackage rreq(10, 1, 12, 3, 2, 5, 4);
    node.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(1, cb2Cnt);
    TEST_ASSERT_EQUAL(12, node.seqNo);
    TEST_ASSERT_EQUAL(1, cbRREP.destinationID);
    TEST_ASSERT_EQUAL(12, cbRREP.destinationSeqNo);
    TEST_ASSERT_EQUAL(0, cbRREP.hopCount);
    TEST_ASSERT_EQUAL(3, cbRREP.originID);
    TEST_ASSERT_EQUAL(1, cbRREP.lastID);
    TEST_ASSERT_EQUAL(ACTIVE_ROUTE_TIMEOUT, cbRREP.lifetime);
}
 
/**
 6.6.2. Route Reply Generation by an Intermediate Node

   If the node generating the RREP is not the destination node, but
   instead is an intermediate hop along the path from the originator to
   the destination, it copies its known sequence number for the
   destination into the Destination Sequence Number field in the RREP
   message.

   The intermediate node updates the forward route entry by placing the
   last hop node (from which it received the RREQ, as indicated by the
   source IP address field in the IP header) into the precursor list for
   the forward route entry -- i.e., the entry for the Destination IP
   Address.  The intermediate node also updates its route table entry
   for the node originating the RREQ by placing the next hop towards the
   destination in the precursor list for the reverse route entry --
   i.e., the entry for the Originator IP Address field of the RREQ
   message data.

   The intermediate node places its distance in hops from the
   destination (indicated by the hop count in the routing table) Count
   field in the RREP.  The Lifetime field of the RREP is calculated by
   subtracting the current time from the expiration time in its route
   table entry.
*/
void test_reply_with_rrep_when_known_route() {
    cb2Cnt = 0;
    // Generate RREP if it is destination
    AODVNode node(66);
    auto oldSq = node.seqNo;
    node.setSendNeighbourRREPCallback(&cb2);
    node.routeTable.table.push_front(Route(1, 13, 5, 2, millis() + 1000));

    RREQPackage rreq(10, 1, 12, 3, 2, 5, 4);
    node.receiveRREQ(rreq);
    TEST_ASSERT_EQUAL(1, cb2Cnt);
    TEST_ASSERT_EQUAL(1, cbRREP.destinationID);
    TEST_ASSERT_EQUAL(13, cbRREP.destinationSeqNo);
    TEST_ASSERT_EQUAL(5, cbRREP.hopCount);
    TEST_ASSERT_EQUAL(3, cbRREP.originID);
    TEST_ASSERT_EQUAL(66, cbRREP.lastID);
    TEST_ASSERT(millis() + ACTIVE_ROUTE_TIMEOUT > cbRREP.lifetime);
    TEST_ASSERT(millis() < cbRREP.lifetime);

    // Test precursor lists
    auto rt = node.routeTable.findByDestinationID(1);
    TEST_ASSERT_EQUAL(1, rt->precursors.size());
    TEST_ASSERT_EQUAL(4, (*rt->precursors.begin()));
    auto rt2 = node.routeTable.findByDestinationID(3);
    TEST_ASSERT_EQUAL(1, rt2->precursors.size());
    TEST_ASSERT_EQUAL(2, (*rt2->precursors.begin()));

    rreq.rreqID = 11;
    rt = node.routeTable.findByDestinationID(1);
    TEST_ASSERT_EQUAL(1, rt->precursors.size());
    TEST_ASSERT_EQUAL(4, (*rt->precursors.begin()));
    rt2 = node.routeTable.findByDestinationID(3);
    TEST_ASSERT_EQUAL(1, rt2->precursors.size());
    TEST_ASSERT_EQUAL(2, (*rt2->precursors.begin()));
}

/**
6.7. Receiving and Forwarding Route Replies

   NEEDED: if route is unkown/damaged
   When a node receives a RREP message, it searches (using longest-
   prefix matching) for a route to the previous hop.  If needed, a route
   is created for the previous hop, but without a valid sequence number
   (see section 6.2).  
   
   Next, the node then increments the hop count
   value in the RREP by one, to account for the new hop through the
   intermediate node.  Call this incremented value the "New Hop Count".
   Then the forward route for this destination is created if it does not
   already exist.  Otherwise, the node compares the Destination Sequence
   Number in the message with its own stored destination sequence number
   for the Destination IP Address in the RREP message.  Upon comparison,
   the existing entry is updated only in the following circumstances:

   (i)       the sequence number in the routing table is marked as
             invalid in route table entry.

   (ii)      the Destination Sequence Number in the RREP is greater than
             the node's copy of the destination sequence number and the
             known value is valid, or

   (iii)     the sequence numbers are the same, but the route is is
             marked as inactive, or

   (iv)      the sequence numbers are the same, and the New Hop Count is
             smaller than the hop count in route table entry.

   If the route table entry to the destination is created or updated,
   then the following actions occur:

   -  the route is marked as active,

   -  the destination sequence number is marked as valid,

   -  the next hop in the route entry is assigned to be the node from
      which the RREP is received, which is indicated by the source IP
      address field in the IP header,

   -  the hop count is set to the value of the New Hop Count,

   -  the expiry time is set to the current time plus the value of the
      Lifetime in the RREP message,

   -  and the destination sequence number is the Destination Sequence
      Number in the RREP message.

   The current node can subsequently use this route to forward data
   packets to the destination.
   */
void test_receive_rrep() {
    AODVNode node(10);
    // RREPPackage(uint32_t destID, uint32_t destSeqNo, uint32_t origID,
    //        uint32_t hopC, uint32_t lID, uint32_t life)
    RREPPackage rrep(1, 2, 3, 4, 9, ACTIVE_ROUTE_TIMEOUT);

    // Node should have valid route to 3 from when RREQ passed
    // Route(uint32_t destID, uint32_t destSeqNo, uint32_t hopC, uint32_t hopID, uint32_t ltime)
    Route rreqRoute(3, 11, 2, 20, ACTIVE_ROUTE_TIMEOUT);
    node.routeTable.table.push_front(rreqRoute);
    node.setSendNeighbourRREPCallback(&cb2);
    cb2Cnt = 0;
    cbRREP.hopCount = 1000;

    node.receiveRREP(rrep);
    auto rt = node.routeTable.findByDestinationID(1);
    TEST_ASSERT_EQUAL(5, cbRREP.hopCount);
    TEST_ASSERT_EQUAL(1, cb2Cnt);
    TEST_ASSERT_EQUAL(9, rt->nextHopID);
    TEST_ASSERT_EQUAL(5, rt->hopCount);
    TEST_ASSERT(rt->lifetime > millis());
    TEST_ASSERT(rt->lifetime < millis() + ACTIVE_ROUTE_TIMEOUT);
    TEST_ASSERT_EQUAL(2, rt->destinationID);

    // Test route updated only when needed (maybe we can defer this test to
    // tryUpdate?)

    //TODO refactor rreq and rrep to use same function for adding route 
    TEST_ASSERT(false);
}

/**
   If the current node is not the node indicated by the Originator IP
   Address in the RREP message AND a forward route has been created or
   updated as described above, the node consults its route table entry
   for the originating node to determine the next hop for the RREP
   packet, and then forwards the RREP towards the originator using the
   information in that route table entry.  If a node forwards a RREP
   over a link that is likely to have errors or be unidirectional, the
   node SHOULD set the 'A' flag to require that the recipient of the
   RREP acknowledge receipt of the RREP by sending a RREP-ACK message
   back (see section 6.8).

   When any node transmits a RREP, the precursor list for the
   corresponding destination node is updated by adding to it the next
   hop node to which the RREP is forwarded.  Also, at each node the
   (reverse) route used to forward a RREP has its lifetime changed to be
   the maximum of (existing-lifetime, (current time +
   ACTIVE_ROUTE_TIMEOUT).  Finally, the precursor list for the next hop
   towards the destination is updated to contain the next hop towards
   the source.
*/
void test_forward_rrep() {
    // Do not forward if destination
    // Update precursor list
    //
    TEST_ASSERT(false);
}

void setup() {
    UNITY_BEGIN();    // IMPORTANT LINE!
}

void loop() {
    RUN_TEST(test_discard_rreq);
    RUN_TEST(test_update_routetable_on_rreq);
    RUN_TEST(test_forward_rreq);
    RUN_TEST(test_reply_with_rrep);
    RUN_TEST(test_reply_with_rrep_when_destination);
    RUN_TEST(test_reply_with_rrep_when_known_route);

    RUN_TEST(test_receive_rrep);
    RUN_TEST(test_forward_rrep);
    UNITY_END(); // stop unit testing
}

#endif
