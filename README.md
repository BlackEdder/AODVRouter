# aodvRouting

Adhoc On demand Distance Vector Routing. This library provides the routing tables and packages to add AODV Routing to your project.

I have no further need for this library at this time and will not be finishing it. It might be of use for someone else though as a starting point and I will leave it here. If you are interested in finishing this library then I will be happy to help you. Current status of the library is about 75% done. The library has been developed straight from the spec, with unit tests covering all the details. The main part of the AODV protocol relies on correct bookkeeping and the tests ensure that this is the case.

I added a couple of failing unit tests that show where I was at implementing the algorithm. I suggest starting with implementing `receiveRREP` if you want to carry on development.

## Platforms

Currently this is only tested on espressif8266 and native, but it should be compatible with most (arduino-like) platforms.

## Design

This library is meant to be a mesh independent routing library. It does all the bookkeeping needed for AODV routing (like keeping track of the routing table and updating it when needed). It relies on the user to provide callbacks that will actually send the packages (serialize/deserialize) and when the mesh receives a message it needs to forward it to the `AODVNode` to deal with.
