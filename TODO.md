- Implement the rfc document step by step
    - Add new header for package types
    - Just add every step they talk about one by one


- At a receiveRREQ function that does something smart when
- At testing for “whole network”
- Probably need list that keeps track of whether an package has been received. If not the update the list (seqNo).
- At sendRREQ (which probably needs to timeout at some point. We probably need a waiting list that contains the destination, the time the RREQ was seen, number of retries for RREQ and the message to send when it is recieved. This list will sometimes need to be purged.
- Allow payload for a message through void pointers, example:
    `void doAfter(void *my_param) { int x = *(int *)my_param; }` which is called as `doAfter((void *)&input_param)`
- The user provided send to specific neighbour callback should return error if neighbour is not reachable

RREQ questions

- On receiving a rreq you add a reverse route to your table. Can this reverse route subsequently be used if this node needs to send a message?
- Apparently should only reply if we know the route and destination seq No in route table is equal or higher than in RREQ, this seems very weird to me, because how can it be equal or higher. Although maybe this sequence number comes from the reply package and is the one we send as well. Still I cannot see how loops can form if we make sure we don’t handle the same flood message twice.

# Testing

- Setup a random network as a unittest. Make sure they are all connected by iteratively add a node and connect it to 1-3 already existing nodes. We can then test send and receive, broadcasting. What happens when links reconnect to others (here there might be a small change of islands forming)
