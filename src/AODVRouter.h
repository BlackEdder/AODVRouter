#ifndef   _AODV_ROUTER_H_
#define   _AODV_ROUTER_H_

#include <list>

typedef uint8_t seq_t;
typedef uint32_t node_id_t;
typedef uint32_t node_time_t;

/**
 * Packages
 */
struct RREQPackage {
    node_id_t sourceID;
    seq_t sourceSeqNo;
    seq_t broadcastID;
    node_id_t destinationID;
    seq_t destinationSeqNo;
    uint8_t hopCount;
};

struct RREPPPackage {
    node_id_t sourceID; // Source of the reply
    node_id_t destinationID; // The destination, i.e. source of the RREQ
    seq_t destinationSeqNo;
    uint8_t hopCount;
    node_time_t lifeTime; // Holds the time it was send
};

// Broken link
//
// Any route that has lastID as nextHopID and one of the invalidIDs will
// be deleted. If we don't have any like those we don't need to forward it
// further.
//
// If we were waiting for a RREP we need to resend RREQ with an 
// destinationSeqNo higher than the one in the package
struct RRERPackage {
    node_id_t lastID; 
    node_id_t destinationID; // Original desitnation 
    seq_t destinationSeqNo;
    std::list<node_id_t> invalidDestinationIDs;
};

struct ReverseRoute {
    node_id_t destinationID;
    node_id_t nextHopID;
    uint8_t hopCount;
    seq_t SeqNo; // Holds the source sequence number
};
/*
struct Route {
    node_id_t destinationID;
    node_id_t nextHopID;
    uint8_t destinationSeqNo;
    uint8_t hopCount;
    node_time_t lifeTime;
};
*/

struct Route {
    node_time_t timeOut = 1000*1000*600; // 10 minutes
    node_id_t destinationID;
    node_id_t nextHopID;
    uint8_t destSeqNo;
    // Last time updated 
    node_time_t updateTime;

    Route(node_id_t destID, node_id_t nextID, uint8_t seqNo, node_time_t update) {
        destinationID = destID;
        nextHopID = nextID;
        destSeqNo = seqNo;
        updateTime = update;
    }

    Route() {}

    bool expired() {
        return false;
    }
};

struct RouteTable {
    std::list<Route> routeTable;

    void insert(Route r) {
        routeTable.push_back(r);
    }

    Route* findRouteToDestination(const node_id_t destination, 
            const node_time_t currentTime) {
        std::list<Route>::iterator connection = routeTable.begin();
        while ( connection != routeTable.end() ) {

            if (connection->destinationID == destination) {  // check direct connections

                // TODO: This is save for overflow, but might mean that routes are kept around multiple overflow rounds, because they don't get purged
                // TODO: If we sent we should update the time (according to the presentation 
                if (currentTime - connection->updateTime >
                        connection->timeOut) {
                    routeTable.erase(connection);
                    return NULL;
                } else {
                    return connection;
                }
            }
            connection++;
        }
        return NULL;
    }
};

class AODVRouter {
    node_id_t nodeID;
    seq_t seqNo; // Incremented when RREP is send?
    seq_t broadcastID; // Incremented whenever RREQ is send



    RouteTable routeTable;
    void sendMessage(const node_id_t destination, 
            const String &message, const node_time_t currentTime) {
        //if (destination == nodeID)
        //    receivedMessageCB(message);

        Route* route = routeTable.findRouteToDestination(destination,
                currentTime);
        /*if (route != NULL) {
            sendNeighbourCB(route.nextHopID, destinationID, message);
        } else {
            sendRouteReq();
        }*/
    }

    private:
    void sendRREQ(node_id_t destinationID, seq_t destinationSeqNo) {
        // TODO: is there a difference between seqID and broadcastSeqNo?
        // Check the youtube video for details
        // IMPL: call sendRREQCB(nodeID, seqID, destinationID, 
        // destinationSeqNo, broadcastSeqNo)
        //++broadcastSeqNo;
    }

    void receiveRREQ(node_id_t sourceID, seq_t seqID, 
            node_id_t destinationID, 
        seq_t destinationSeqNo, seq_t broadcastSeqNo, uint8_t hopCount) {
        // Check if already seen based on source and broadcastSNo
        // Set up Reverse route
        // Check whether we are destination, if so send reply package
        // Check whether we know the route, (and destination seq ID is equalt to or higher than current destinationseq??) if so send reply package
        // Forward to all neighbours using sendRREQCB, while increasing hop
        // count
    }
};
#endif
