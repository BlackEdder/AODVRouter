#ifndef   _AODV_NODE_H_
#define   _AODV_NODE_H_

#include <list>

#include "AODVPackages.h"
#include "AODVUtils.h"
#include "AODVRoute.h"

class AODVNode {
    public:
    uint32_t id;
    uint32_t seqNo = 1; // Value of 0 indicates invalid

    RouteTable routeTable;

    AODVNode(uint32_t nodeID) {
        id = nodeID;
    }

    /**
     * Below are mostly private, but set public here to enable testing
     */
    std::list<RREQCacheEntry> rreqCache;

    void initializeRREQ() {
        ++seqNo;
    }

    /** 
     * Initialaze RREP when receiving a RREQ with this node as destination
     *
     * Need to pass the destination sequence number in the rreq
     */
    void initializeRREP(const RREQPackage &rreq) {
        if (seqNo_lower_than(seqNo, rreq.destinationSeqNo))
            seqNo = rreq.destinationSeqNo;
        if (sendNeighbourRREPCallback)
            sendNeighbourRREPCallback(rreq.lastID,
                RREPPackage(rreq.destinationID, seqNo,
                    rreq.originID, 0, id, ACTIVE_ROUTE_TIMEOUT));
    }

    void (*sendNeighboursRREQCallback)(const RREQPackage &rreq) = NULL;
    void setSendNeighboursRREQCallback( void(*callBack)(const RREQPackage &rreq) ) {
        sendNeighboursRREQCallback = callBack;
    }

    void (*sendNeighbourRREPCallback)(uint32_t neighbourID, const RREPPackage &rrep) = NULL;
    void setSendNeighbourRREPCallback( 
            void(*callBack)(uint32_t neighbourID, const RREPPackage &rrep) ) {
        sendNeighbourRREPCallback = callBack;
    }

    void receiveRREQ(RREQPackage &rreq) {
        bool newRREQ = true;

        auto entry = rreqCache.begin();
        auto cTime = millis();
        while (newRREQ && entry != rreqCache.end()) {
            if (entry->match(rreq, cTime))
                newRREQ = false;
            ++entry;
        }

        // This always happens, because new RREQ can have a shorter route than the old one
        // NEEDED should ideally also update the lifetime, irregardeless, maybe should be 
        // optional flag to tryUpdate
        ++rreq.hopCount;
        routeTable.tryUpdate(rreq.originID, rreq.originSeqNo, rreq.hopCount,
                rreq.lastID, routeTable.calculateExpireTime(millis()));
        //bool tryUpdate( uint32_t destinationID, uint32_t destinationSeqNo, uint32_t hopCount, 
        //        uint32_t nextHopID, uint32_t lifetime) {
        if (newRREQ) {
            rreqCache.push_front(RREQCacheEntry(rreq, cTime));

            //RREPPackage(uint32_t destID, uint32_t destSeqNo, uint32_t origID,
            //  uint32_t hopC, uint32_t lID, uint32_t life) {
            if (rreq.destinationID == id)
                initializeRREP(rreq);

            auto route = routeTable.findByDestinationID(rreq.destinationID);
            bool forward = true;
            if (route) {
                if (seqNo_lower_than(rreq.destinationSeqNo, 
                            route->destinationSeqNo)) {
                    rreq.destinationSeqNo = route->destinationSeqNo;
                    if (route->active(millis()) && 
                            sendNeighbourRREPCallback) {
                        route->addPrecursor(rreq.lastID);
                        // TODO Somehow get the route from tryUpdate
                        auto rt = routeTable
                            .findByDestinationID(rreq.originID);
                        rt->addPrecursor(route->nextHopID);

                        sendNeighbourRREPCallback(route->nextHopID,
                                RREPPackage(rreq.destinationID, 
                                    route->destinationSeqNo,
                                    rreq.originID, route->hopCount, 
                                    id, 
                                    route->lifetime - millis()));
                        forward = false;
                    }
                }
            } 
            
            if (forward && sendNeighboursRREQCallback) {
                rreq.lastID = id;
                sendNeighboursRREQCallback(rreq);
            }
        }
   }

    void receiveRREP(const RREPPackage &rrep) {
        // This function should work very similar to receiveRREQ above


        //bool tryUpdate( uint32_t destinationID, uint32_t destinationSeqNo, uint32_t hopCount, 
        //        uint32_t nextHopID, uint32_t lifetime) {
        routeTable.tryUpdate(rrep.destinationID, rrep.destinationSeqNo, rrep.hopCount,
                rrep.lastID, millis() + rrep.lifetime);
    }
};

#endif
