#ifndef   _AODV_ROUTE_H_
#define   _AODV_ROUTE_H_

#include <list>

#include <AODVUtils.h>

struct Route {
    uint32_t destinationID;
    uint32_t destinationSeqNo;
    uint32_t hopCount;
    uint32_t nextHopID;
    uint32_t lifetime; // Time after which route is not valid anymore

    // Ideally should use some kind of set
    std::list<uint32_t> precursors;

    Route(uint32_t destID, uint32_t destSeqNo, uint32_t hopC, 
            uint32_t hopID, uint32_t ltime) {
        destinationID = destID;
        destinationSeqNo = destSeqNo;
        hopCount = hopC;
        nextHopID = hopID;
        lifetime = ltime;
    }

    Route() {}

    bool active(uint32_t currentTime) {
        return lifetime >= currentTime;
    }

    void addPrecursor(uint32_t precursor) {
        bool add = true;
        for (auto it = precursors.begin(); it != precursors.end(); ++it) {
            if ((*it) == precursor) {
                add = false;
                break;
            }
        }
        if (add)
            precursors.push_front(precursor);
    }
};

#define ACTIVE_ROUTE_TIMEOUT 60*1000*10 // Ten minutes

struct RouteTable {
    std::list<Route> table;

    // Update if the route is newer or shorter. Note that life time should be the time
    // after which the route will expire. Generally this is the current time + the time for
    // which the route is valid. If unknown you can call calculateExpireTime to 
    // calculate it based the currentTime
    bool tryUpdate( uint32_t destinationID, uint32_t destinationSeqNo, uint32_t hopCount, 
            uint32_t nextHopID, uint32_t lifetime) {
        auto it = table.begin();
        while (it != table.end()) {
            if (it->destinationID == destinationID) {
                if (seqNo_lower_than(it->destinationSeqNo, destinationSeqNo) ||
                        (it->destinationSeqNo == destinationSeqNo && it->hopCount > hopCount)) {
                    it->destinationSeqNo = destinationSeqNo;
                    it->hopCount = hopCount;
                    it->nextHopID = nextHopID;
                    it->lifetime = lifetime;
                    return true;
                } else 
                    return false;
            }
            ++it;
        }
        table.push_back(Route(destinationID, destinationSeqNo, hopCount, nextHopID, lifetime));
        return true;
    }

    Route* findByDestinationID(uint32_t destinationID) {
        for (auto it = table.begin(); it != table.end(); ++it) {
            if (it->destinationID == destinationID)
                return it;
        }
        return NULL;
    }

    // Return the time a route will expire based on the current time.
    uint32_t calculateExpireTime(uint32_t currentTime) {
        return currentTime + ACTIVE_ROUTE_TIMEOUT;
    }
};
#endif
