#ifndef   _AODV_PACKAGES_H_
#define   _AODV_PACKAGES_H_

#include<list>

/* Here we define all the needed packages. Note that in contrast to the official documentation we
 * also pass along the last node id (and lastNodeSeqNo?). In the original documentation this seems
 * to be part of the header, but not here.
 */

/*
 * Route Request package
 *
 * RREQ packet. Below is the official documentation. We do not currently support
 * all flags, but the main ones we do.
 *
 *
 * From the official spec:
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |J|R|G|D|U|   Reserved          |   Hop Count   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                            RREQ ID                            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Destination IP Address                     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                  Destination Sequence Number                  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Originator IP Address                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                  Originator Sequence Number                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Type           1

      J              Join flag; reserved for multicast.

      R              Repair flag; reserved for multicast.

      G              Gratuitous RREP flag; indicates whether a
                     gratuitous RREP should be unicast to the node
                     specified in the Destination IP Address field (see
                     sections 6.3, 6.6.3).

      D              Destination only flag; indicates only the
                     destination may respond to this RREQ (see
                     section 6.5).

      U              Unknown sequence number; indicates the destination
                     sequence number is unknown (see section 6.3).

      Reserved       Sent as 0; ignored on reception.

      Hop Count      The number of hops from the Originator IP Address
                     to the node handling the request.


      RREQ ID        A sequence number uniquely identifying the
                     particular RREQ when taken in conjunction with the
                     originating node's IP address.

      Destination IP Address
                     The IP address of the destination for which a route
                     is desired.

      Destination Sequence Number
                     The latest sequence number received in the past
                     by the originator for any route towards the
                     destination.

      Originator IP Address
                     The IP address of the node which originated the
                     Route Request.

      Originator Sequence Number
                     The current sequence number to be used in the route
                     entry pointing towards the originator of the route
                     request.
*/
struct RREQPackage {
    uint32_t rreqID; // Also often known as broadcast ID
    uint32_t destinationID;
    uint32_t destinationSeqNo;
    uint32_t originID;
    uint32_t originSeqNo;
    uint32_t hopCount;
    uint32_t lastID;

    RREQPackage(uint32_t rID, uint32_t destID, uint32_t destSeqNo, uint32_t origID,
            uint32_t origSeqNo, uint32_t hopC, uint32_t lID) {
        rreqID = rID;
        destinationID = destID;
        destinationSeqNo = destSeqNo;
        originID = origID;
        originSeqNo = origSeqNo;
        hopCount = hopC;
        lastID = lID;
    }

    RREQPackage() {}
};

#define PATH_DISCOVERY_TIME 60*1000*5 // Five minutes
struct RREQCacheEntry {
    uint32_t rreqID; // Also often known as broadcast ID
    uint32_t originID;
    uint32_t lifetime;

    RREQCacheEntry(const RREQPackage &rreq, uint32_t currentTime) {
        lifetime = currentTime + PATH_DISCOVERY_TIME;
        rreqID = rreq.rreqID;
        originID = rreq.originID;
    }

    RREQCacheEntry() {}

    bool match(const RREQPackage &rreq, uint32_t currentTime) {
        if (valid(currentTime))
            return (rreq.rreqID == rreqID && rreq.originID == originID);
        return false;
    }

    bool valid(uint32_t currentTime) {
        return currentTime <= lifetime;
    }
};

/** Route reply package (RREP)
 *
 *
 * From the official spec:
5.2. Route Reply (RREP) Message Format

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |R|A|    Reserved     |Prefix Sz|   Hop Count   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Destination IP address                    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                  Destination Sequence Number                  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Originator IP address                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                           Lifetime                            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   The format of the Route Reply message is illustrated above, and
   contains the following fields:

      Type          2

      R             Repair flag; used for multicast.

      A             Acknowledgment required; see sections 5.4 and 6.7.

      Reserved      Sent as 0; ignored on reception.


      Prefix Size   If nonzero, the 5-bit Prefix Size specifies that the
                    indicated next hop may be used for any nodes with
                    the same routing prefix (as defined by the Prefix
                    Size) as the requested destination.

      Hop Count     The number of hops from the Originator IP Address
                    to the Destination IP Address.  For multicast route
                    requests this indicates the number of hops to the
                    multicast tree member sending the RREP.

      Destination IP Address
                    The IP address of the destination for which a route
                    is supplied.

      Destination Sequence Number
                    The destination sequence number associated to the
                    route.

      Originator IP Address
                    The IP address of the node which originated the RREQ
                    for which the route is supplied.

      Lifetime      The time in milliseconds for which nodes receiving
                    the RREP consider the route to be valid.

   Note that the Prefix Size allows a subnet router to supply a route
   for every host in the subnet defined by the routing prefix, which is
   determined by the IP address of the subnet router and the Prefix
   Size.  In order to make use of this feature, the subnet router has to
   guarantee reachability to all the hosts sharing the indicated subnet
   prefix.  See section 7 for details.  When the prefix size is nonzero,
   any routing information (and precursor data) MUST be kept with
   respect to the subnet route, not the individual destination IP
   address on that subnet.

   The 'A' bit is used when the link over which the RREP message is sent
   may be unreliable or unidirectional.  When the RREP message contains
   the 'A' bit set, the receiver of the RREP is expected to return a
   RREP-ACK message.  See section 6.8.
*/
struct RREPPackage {
    uint32_t destinationID; // Destination this RREP is for
    uint32_t destinationSeqNo; // I have the feeling this is not actually needed
    uint32_t originID;
    uint32_t hopCount;
    uint32_t lastID;
    uint32_t lifetime;

    RREPPackage(uint32_t destID, uint32_t destSeqNo, uint32_t origID,
            uint32_t hopC, uint32_t lID, uint32_t life) {
        destinationID = destID;
        destinationSeqNo = destSeqNo;
        originID = origID;
        hopCount = hopC;
        lastID = lID;
        lifetime = life;
    }

    RREPPackage() {}
};

/*
 Route Error (RERR) Message Format

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |N|          Reserved           |   DestCount   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |            Unreachable Destination IP Address (1)             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |         Unreachable Destination Sequence Number (1)           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-|
   |  Additional Unreachable Destination IP Addresses (if needed)  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |Additional Unreachable Destination Sequence Numbers (if needed)|
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   The format of the Route Error message is illustrated above, and
   contains the following fields:

      Type        3

      N           No delete flag; set when a node has performed a local
                  repair of a link, and upstream nodes should not delete
                  the route.

      Reserved    Sent as 0; ignored on reception.

      DestCount   The number of unreachable destinations included in the
                  message; MUST be at least 1.

      Unreachable Destination IP Address
                  The IP address of the destination that has become
                  unreachable due to a link break.

      Unreachable Destination Sequence Number
                  The sequence number in the route table entry for
                  the destination listed in the previous Unreachable
                  Destination IP Address field.

   The RERR message is sent whenever a link break causes one or more
   destinations to become unreachable from some of the node's neighbors.
   See section 6.2 for information about how to maintain the appropriate
   records for this determination, and section 6.11 for specification
   about how to create the list of destinations.
 */
struct RRERPackage {
    uint32_t lastID; // Destination this RREP is for

    std::list<uint32_t> destinationIDs;
    std::list<uint32_t> destinationSeqNos;

    RRERPackage(uint32_t destID, uint32_t destSeqNo, uint32_t lID) {
        destinationIDs.push_front(destID);
        destinationSeqNos.push_front(destSeqNo);
        lastID = lID;
    }

    RRERPackage() {}
};

#endif
