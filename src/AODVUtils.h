#ifndef   _AODV_UTILS_H_
#define   _AODV_UTILS_H_

bool seqNo_lower_than(uint32_t lowerSeqNo, uint32_t seqNo) {
    return (int)seqNo - (int) lowerSeqNo > 0;
}

#endif
